import QtQuick 2.4
import DgQuick 1.0
import dgdesignexample 1.0

Rectangle {
    property alias scroll: scroll
    property alias mapFlickable: mapFlickable
    width: Constants.width
    height: 600
    color: "white"

    Text {
        text: "Page 2"
        font.bold: false
        font.pixelSize: 40
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 50
        horizontalAlignment: Text.AlignHCenter
    }

    Flickable {
        id: mapFlickable
        x: 130
        y: 140
        width: 1022
        height: 400
        clip: true
        contentX: mapImage.width / 2 - mapFlickable.width / 2
        contentY: mapImage.height / 2 - mapFlickable.height / 2
        contentWidth: mapImage.width
        contentHeight: mapImage.height
        Image {
            id: mapImage
            source: "assets/map.png"
            Rectangle {
                anchors.fill: parent
                color: "transparent"
                border.color: "red"
                border.width: 4
            }
        }
    }

    EyeArea {
        id: scroll
        x: mapFlickable.x
        y: mapFlickable.y
        width: mapFlickable.width
        height: mapFlickable.height
        scrollEnabled: true
        scrollDeadSubArea: "0.3,0.3,0.4x0.4"
        horizontalMagnification: 1.1
        verticalMagnification: 1.1
    }
}
