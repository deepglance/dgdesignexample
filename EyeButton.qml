import QtQuick 2.4
import DgQuick 1.0

EyeButtonForm {
   id: root
   onEnabledChanged: {
       if(enabled) {
           state = "";
       }
       else {
           state = "Disabled";
       }
   }
   Connections {
        target: eyeArea
        onAction: {
            action();
        }
    }
}
