import QtQuick 2.4

Page2Form {
    Connections {
        target: scroll
        onScroll: {
            mapFlickable.contentX = Math.max(0, Math.min(mapFlickable.contentWidth - mapFlickable.width, mapFlickable.contentX + (dx * 3000)))
            mapFlickable.contentY = Math.max(0, Math.min(mapFlickable.contentHeight - mapFlickable.height, mapFlickable.contentY - (dy * 3000)))
        }
    }
}
