import QtQuick 2.4

Page1Form {
    Connections {
        target: button1
        onAction: {
            button1.state = "Selected"
            button2.state = ""
        }
    }
    Connections {
        target: button2
        onAction: {
            button1.state = ""
            button2.state = "Selected"
        }
    }
}
