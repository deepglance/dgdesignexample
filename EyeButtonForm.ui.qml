import QtQuick 2.4
import DgQuick 1.0

Rectangle {
    id: root
    property alias eyeArea: eyeArea
    property alias text: label.text
    signal action

    color: "#5a98ee"
    width: 200
    height: 200

    Text {
        id: label
        color: "#ffffff"
        text: "Button"
        font.pixelSize: 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }

    Rectangle {
        id: actionDwellFeedbackack
        anchors.centerIn: parent
        z: 1
        width: parent.width * (1 - eyeArea.actionProgress)
        height: parent.height * (1 - eyeArea.actionProgress)
        color: "#55ffffff"
        radius: width * 0.5
        visible: state !== "Selected" && eyeArea.eyeFocus
    }

    Rectangle {
        id: selectionFrame
        anchors.fill: parent
        border.width: 8
        border.color: "lime"
        color: "#00000000"
        visible: false
    }

    EyeArea {
        id: eyeArea
        anchors.fill: parent
        enabled: parent.enabled
        actionDwellTime: 750
    }

    states: [
        State {
            name: "Disabled"

            PropertyChanges {
                target: root
                color: "#AA5a98ee"

            }
            PropertyChanges {
                target:  label
                color: "#AAffffff"
            }
        },
        State {
            name: "Selected"

            PropertyChanges {
                target: selectionFrame
                visible: true
            }
        }
    ]
}
