import QtQuick 2.10
import QtQuick.Controls 2.2
import QtQuick.Window 2.10
import DgQuick 1.0
import dgdesignexample 1.0

Item {
    width: Constants.width
    height: Constants.height
    SwipeView {
        id: swipeView
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: navigator.top
        currentIndex: 0
        Page1 {
        }
        Page2 {
        }
    }

    PageIndicator {
        count: swipeView.count
        currentIndex: swipeView.currentIndex
        anchors.bottom: swipeView.bottom
        anchors.horizontalCenter: swipeView.horizontalCenter
    }

    Navigator {
        id: navigator
        height: 200
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }
}
